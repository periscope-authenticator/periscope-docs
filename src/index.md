---
layout: home
hero:
    name: Periscope
    text: Browser-based authentication for command-line tools
    tagline: Documentation for Periscope Authenticator
    image:
        src: /Periscope_whole body-01.png
        alt: Periscope
    actions:
        - theme: brand
          text: User Guide →
          link: /user-guide/
---


import { defineConfig } from 'vitepress'
import { description } from '../../package'

export default defineConfig({
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Periscope',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],
  outDir: "../public",

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    logo: '/favicon.ico',
    editLink: {
        pattern: 'https://gitlab.com/periscope-authenticator/periscope-docs/-/edit/main/src/:path',
        text: 'Edit this page on GitLab',
    },
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'User Guide',
        link: '/user-guide/',
      },
      {
        text: 'Dev Guide',
        link: '/dev-guide/'
      },
      {
        text: 'Periscope Gitlab',
        link: 'https://gitlab.com/periscope-authenticator/periscope'
      }
    ],
    sidebar: {
      'dev-guide': [],
      '/guide/': [
        {
          title: 'Guide',
          collapsable: false,
          children: [
            '',
            'using-vue',
          ]
        }
      ],
    }
  },
});

